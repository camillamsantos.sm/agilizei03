**Agilizei

Semana Agilizei 3.0

Cypress na prática, do zero à integração contínua.**



Dia 1 – Abertura do evento e primeiros passos com o Cypress

Links adicionais:
Repositório oficial do Cypress: https://github.com/cypress-io/cypress
Cypress Essencial Mindmap – https://github.com/samlucax/cypress-essencial-mindmap
Cypress Essencial Mindmap – Vídeo
Cypress FAQ – https://github.com/samlucax/cypress-faq
Awesome Cypress – https://github.com/brunopulis/awesome-cypress
Cases do Cypress – https://www.cypress.io/case-studies

**Pessoas que você deve seguir e acompanhar**

Gleb Bahmutov – VP de Engenharia, e um dos que mais compartilha novidades – https://github.com/bahmutov
Brian Mann – Fundador e CEO, responde constantemente as issues – https://github.com/brian-mann
Jennifer Shehane – Dev / QA, responde a maioria das issues – https://github.com/jennifer-shehane
Aproveite e me siga no Github também – https://github.com/samlucax

**Comunidades que você deve participar**

Cypress BR no Telegram – t.me/cypress_br
Canal Agilizei – t.me/agilizei
Gitter Cypress – https://gitter.im/cypress-io/cypress

Dia 2 – Utilizando Cypress na prática
Links adicionais:
Aplicação de testes: https://devfinance-agilizei.netlify.app

Dia 3 – Evoluindo a estrutura
Links adicionais:
Gist com as funções úteis: https://gist.github.com/samlucax/a3b0b1fb258e7e3f7f6d38daea6ca7c5


Dia 4 – Cypress Dashboard & Github Actions
Links adicionais:
Documentação do Dashboard, visão geral: Link
Documentação do Dashboard, visão completa e tutoriais: Link
Roadmap do Cypress Dashboard: Link
Gist com o workflow e as actions implementadas: https://gist.github.com/samlucax/665f62fabd4dc31435571c0371613ac7
