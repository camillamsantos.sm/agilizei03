/// <reference types="cypress" />

import { format, prepareLocalStorage} from '../support/utils'


// cy.viewport
//arquivos de config
//configs por linha de comando

context('Dev Finances Agilizei', () => {
    // hooks
    //trechos que executam antes e depois do teste
    //before -> antes de todos os testes
    //beforeEach -> antes de cada teste
    //after -> depois de todos os testes
    //afterEach -> depois de cada teste

    beforeEach(() => {
        cy.visit('https://devfinance-agilizei.netlify.app', {
            onBeforeLoad: (win) => {
                prepareLocalStorage (win) 
            }        
            })
        
    });
    it('Cadastrar Entradas', () => {
        //entender o fluxo manualmente (acesso do site e recursos usando manualmente)
        //mapear os elementos que vamos interagir
        //descrever a interações com o cypress
        //adicionar as asserções (validações necessárias)

        // Nova transação
        cy.get('#transaction .button').click() // estratégia de busca: id + classe
        //Descrição
        cy.get('#description').type('Mesada') //estratégia de busca: id
        //Valor
        cy.get('[name=amount]').type(12) // estratégia de busca: atributos
        // Data
        cy.get('[type=date]').type('2021-01-09') // estratégia de busca: atributos
        //Botão Cancelar

        //Botão Salvar
        cy.get('button').contains('Salvar').click() //estratégia de busca: tipo e valor

        cy.get('#data-table tbody tr').should('have.length', 3) //pega o elemento da tabela para validar quantas inserções podem ter, no código indicamos 1

    });
    it('Cadastrar Saídas', () => {

        cy.get('#transaction .button').click()
        cy.get('#description').type('Presente')
        cy.get('[name=amount]').type(-12)
        cy.get('[type=date]').type('2021-01-09')
        cy.get('button').contains('Salvar').click()
        cy.get('#data-table tbody tr').should('have.length', 3)
    });

    it('Remover Entradas e Saídas', () => {

        
        //estratégia 1: voltar para o elemento pai, e avançar para um td img e atributo
        cy.get('td.description')
            .contains("Mesada")
            .parent()
            .find('img[onclick*=remove]')
            .click()

        //estratégia 2: buscar todos os irmãos e buscar o que tem img + attr (atributo)
        cy.get('td.description')
            .contains('Suco Kapo')
            .siblings()
            .children('img[onclick*=remove]')
            .click()

        cy.get('#data-table tbody tr').should('have.length', 0)

    });
    it('Validar saldo com diversas transações', () => {

       

        //capturar as linhas com as transações e as colunas com valores
        //capturar o texto dessas colunas
        //formatar os valores da coluna valor

        //somar os valores de entradas e saídas
        //capturar o texto da coluna total
        //comparar o somatório de entradas e despesas com o total
        
        let incomes = 0
        let expenses = 0

        cy.get('#data-table tbody tr')
        .each (($el,index, $list) => {

            cy.get ($el).find('td.income, td.expense').invoke('text').then(text => {
               
                    if(text.includes('-')){
                        expenses = expenses + format (text)
                    } else {
                        incomes = incomes + format (text)
                    }
                    cy.log('entradas', incomes)
                    cy.log('saídas', expenses)
                })


        });

        cy.get ('#totalDisplay').invoke('text').then(text => {
           
            let formattedTotalDisplay = format (text)
            let expectedTotal = incomes + expenses
            expect (formattedTotalDisplay).to.eq(expectedTotal)
        })

    });
});